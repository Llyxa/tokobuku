@extends('admin.layouts.app')
@section('keranjang','active')

@section('title', 'keranjang')

@section('content')
<style>
    .judul{
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .judul:hover{
        overflow: visible;
    }
</style>
    <!-- BEGIN: Content-->
    <div class="app-content content ecommerce-application">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Checkout</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a>
                                    </li>
                                    {{-- <li class="breadcrumb-item"><a href="#">eCommerce</a>
                                    </li> --}}
                                    <li class="breadcrumb-item active">Checkout
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                    <div class="form-group breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="app-todo.html"><i class="mr-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="mr-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="mr-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="mr-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="bs-stepper checkout-tab-steps">
                    <!-- Wizard starts -->
                    <div class="bs-stepper-header">
                        <div class="step" data-target="#step-cart">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather="shopping-cart" class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">Cart</span>
                                    <span class="bs-stepper-subtitle">Your Cart Items</span>
                                </span>
                            </button>
                        </div>
                        <div class="line">
                            <i data-feather="chevron-right" class="font-medium-2"></i>
                        </div>
                        <div class="step" data-target="#step-address">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather="home" class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">Address</span>
                                    <span class="bs-stepper-subtitle">Enter Your Address</span>
                                </span>
                            </button>
                        </div>
                        <div class="line">
                            <i data-feather="chevron-right" class="font-medium-2"></i>
                        </div>
                        <div class="step" data-target="#step-payment">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather="credit-card" class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">Payment</span>
                                    <span class="bs-stepper-subtitle">Select Payment Method</span>
                                </span>
                            </button>
                        </div>
                    </div>
                    <!-- Wizard ends -->

                    <div class="bs-stepper-content">
                        <!-- Checkout Place order starts -->
                        <div id="step-cart" class="content">
                            <div id="place-order" class="list-view product-checkout">
                                <!-- Checkout Place Order Left starts -->
                                <div class="checkout-items">
                                    @foreach ($keranjang as $cart)
                                    <div class="card ecommerce-card ">
                                        <div class="item-img">
                                            <a href="{{route('produk.show', $cart->produk->id)}}">
                                                <div class="" style="margin:15px;">
                                                    <img src="{{ asset ('iniimg/foto/'. $cart->produk->image) }}" alt="{{$cart->produk->judul}}" />
                                                </div>
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <div class="item-name">
                                                {{-- <h6 class="cart-item-title "><a class="text-body" href="{{route('produk.show', $cart->produk->id)}}"> {{ $cart->produk->judul }}</a></h6> --}}
                                                <h1 class="cart-item-title"><a href="{{route('produk.show', $cart->produk->id)}}" class="text-body">{{$cart->produk->judul}}</a></h1>
                                                <span class="item-company">Written By <a href="javascript:void(0)" class="company-name">{{$cart->produk->penulis}}</a></span>
                                                <div class="item-rating">
                                                    <ul class="unstyled-list list-inline">
                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            @if ($cart->produk->stok > 0)
                                                <p class="card-text">Available - <span class="text-success">{{$cart->produk->stok}}</span></p>
                                            @else
                                                <p class="card-text">Not Available
                                            @endif
                                            <h4 class="item-price">Rp. {{number_format($cart->produk->harga)}}</h4>
                                            <div class="cart-item-qty">
                                                <div class="input-group">
                                                    <span class="quantity-title">Qty:</span>
                                                    <input class="touchspin-cart" id="qty" data-id="{{ $cart->id }}" type="number" value="{{ $cart->qty }}" height="1rem">
                                                </div>
                                            </div>
                                            <span class="delivery-date text-muted">Delivery by, Wed Apr 25</span>
                                            <span class="text-success">17% off 4 offers Available</span>
                                        </div>
                                        <div class="item-options text-center a">
                                            <div class="item-wrapper">
                                                <div class="item-cost">
                                                    Subtotal : <br><h4 class="disabled pricee d-inline-block" id="harga-produk"> Rp. {{ number_format(($cart->subtotal)) }}</h4>
                                                    <p class="card-text shipping">
                                                        <span class="badge badge-pill badge-light-success">Free Shipping</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-light mt-1 del" data-id="{{$cart->id}}">
                                                <i data-feather="x" class="align-middle mr-25"></i>
                                                <span>Remove</span>
                                            </button>
                                            <button type="button" class="btn btn-primary btn-cart move-cart">
                                                <i data-feather="heart" class="align-middle mr-25"></i>
                                                <span class="text-truncate">Add to Wishlist</span>
                                            </button>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <!-- Checkout Place Order Left ends -->

                                <!-- Checkout Place Order Right starts -->
                                <div class="checkout-options">
                                    <div class="card">
                                        <div class="card-body">
                                            <label class="section-label mb-1">Options</label>
                                            <div class="coupons input-group input-group-merge">
                                                <input type="text" class="form-control" placeholder="Voucher" aria-label="Coupons" aria-describedby="input-coupons" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text text-primary" id="input-coupons">Apply</span>
                                                </div>
                                            </div>
                                            <hr />
                                            <div class="price-details">
                                                {{-- <h6 class="price-title">Price Details</h6>
                                                <ul class="list-unstyled">
                                                    <li class="price-detail">
                                                        <div class="detail-title">Total MRP</div>
                                                        <div class="detail-amt">$598</div>
                                                    </li>
                                                    <li class="price-detail">
                                                        <div class="detail-title">Bag Discount</div>
                                                        <div class="detail-amt discount-amt text-success">-25$</div>
                                                    </li>
                                                    <li class="price-detail">
                                                        <div class="detail-title">Estimated Tax</div>
                                                        <div class="detail-amt">$1.3</div>
                                                    </li>
                                                    <li class="price-detail">
                                                        <div class="detail-title">EMI Eligibility</div>
                                                        <a href="javascript:void(0)" class="detail-amt text-primary">Details</a>
                                                    </li>
                                                    <li class="price-detail">
                                                        <div class="detail-title">Delivery Charges</div>
                                                        <div class="detail-amt discount-amt text-success">Free</div>
                                                    </li>
                                                </ul> --}}
                                                {{-- <hr /> --}}
                                                <ul class="list-unstyled">
                                                    <li class="price-detail">
                                                        <div class="detail-title detail-total">Total</div>
                                                        <div class="detail-amt font-weight-bolder" id="total-all">Rp. {{ number_format($total) }}</div>
                                                    </li>
                                                </ul>
                                                <button type="button" class="btn btn-primary btn-block btn-next place-order">Checkout</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Checkout Place Order Right ends -->
                                </div>
                            </div>
                            <!-- Checkout Place order Ends -->
                        </div>
                        <!-- Checkout Customer Address Starts -->
                        <div id="step-address" class="content">
                            <form id="checkout-address" class="list-view product-checkout">
                                <!-- Checkout Customer Address Left starts -->
                                <div class="card">
                                    <div class="card-header flex-column align-items-start">
                                        <h4 class="card-title">Alamat Pengiriman</h4>
                                        {{-- <p class="card-text text-muted mt-25">Be sure to check "Deliver to this address" when you have finished</p> --}}
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group mb-2">
                                                    <label for="checkout-name">Nama Lengkap:</label>
                                                    <input type="text" id="checkout-name" class="form-control" name="name" value="" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group mb-2">
                                                    <label for="checkout-number">Nomor Telepon:</label>
                                                    <input type="number" id="checkout-number" class="form-control" name="notelp" placeholder="0123456789" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group mb-2">
                                                    <label for="checkout-apt-number">Provinsi:</label>
                                                    <input type="number" id="checkout-apt-number" class="form-control" name="provinsi" placeholder="Jawa Barat" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group mb-2">
                                                    <label for="checkout-landmark">Kota/Kabupaten:</label>
                                                    <input type="text" id="checkout-landmark" class="form-control" name="kotaOrKab" placeholder="Kota Bandung" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group mb-2">
                                                    <label for="checkout-city">Detail Alamat:</label>
                                                    <input type="text" id="checkout-city" class="form-control" name="det" placeholder="Jl. Jalan No. 8" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group mb-2">
                                                    <label for="checkout-pincode">Patokan:</label>
                                                    <input type="number" id="checkout-pincode" class="form-control" name="patokan" placeholder="Dekat rumah sakit" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group mb-2">
                                                    <label for="checkout-state">Kode Pos:</label>
                                                    <input type="text" id="checkout-state" class="form-control" name="kodepos" placeholder="201301" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group mb-2">
                                                    <label for="add-type">Tipe:</label>
                                                    <select class="form-control" id="add-type">
                                                        <option>Rumah</option>
                                                        <option>Kantor</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <button type="button" class="btn btn-primary delivery-address">Simpan Alamat</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Checkout Customer Address Left ends -->

                                <!-- Checkout Customer Address Right starts -->
                                <div class="customer-card">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">John Doe</h4>
                                        </div>
                                        <div class="card-body actions">
                                            <p class="card-text mb-0">0123456789</p>
                                            <p class="card-text">Jl. Jalan No. 8, Dekat rumah sakit</p>
                                            <p class="card-text">Kota Bandung, 201301</p>
                                            {{-- <p class="card-text">202-555-0140</p> --}}
                                            <button type="button" class="btn btn-primary btn-block btn-next delivery-address mt-2">
                                                Kirim Ke Alamat Ini
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <!-- Checkout Customer Address Right ends -->
                            </form>
                        </div>
                        <!-- Checkout Customer Address Ends -->

                        <!-- Checkout Payment Starts -->
                        <div id="step-payment" class="content">
                            <form id="checkout-payment" class="list-view product-checkout" onsubmit="return false;">
                                <div class="payment-type">
                                    <div class="card">
                                        <div class="card-header flex-column align-items-start">
                                            <h4 class="card-title">Pilih Pembayaran</h4>
                                            {{-- <p class="card-text text-muted mt-25">Be sure to click on correct payment option</p> --}}
                                        </div>
                                        <div class="card-body">
                                            <ul class="other-payment-options list-unstyled">
                                                <li class="py-50">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customColorRadio2" name="paymentOptions" class="custom-control-input" />
                                                        <label class="custom-control-label" for="customColorRadio2"> Credit / Debit / ATM Card </label>
                                                    </div>
                                                </li>
                                                <li class="py-50">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customColorRadio3" name="paymentOptions" class="custom-control-input" />
                                                        <label class="custom-control-label" for="customColorRadio3"> Mobile Banking </label>
                                                    </div>
                                                </li>
                                                <li class="py-50">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customColorRadio5" name="paymentOptions" class="custom-control-input" />
                                                        <label class="custom-control-label" for="customColorRadio5"> Cash On Delivery </label>
                                                    </div>
                                                </li>
                                            </ul>
                                            <hr class="my-2" />
                                            <div class="gift-card mb-25">
                                                <p class="card-text">
                                                    <i data-feather="plus-circle" class="mr-50 font-medium-5"></i>
                                                    <span class="align-middle">Add Gift Card</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="amount-payable checkout-options">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Price Details</h4>
                                        </div>
                                        <div class="card-body">
                                            <ul class="list-unstyled price-details">
                                                <li class="price-detail">
                                                    <div class="details-title">Total Harga</div>
                                                    <div class="detail-amt">
                                                        <strong>Rp. {{ number_format($total) }}</strong>
                                                    </div>
                                                </li>
                                                <li class="price-detail">
                                                    <div class="details-title">Delivery Charges</div>
                                                    <div class="detail-amt discount-amt text-success">Free</div>
                                                </li>
                                            </ul>
                                            <hr />
                                            <ul class="list-unstyled price-details">
                                                <li class="price-detail">
                                                    <div class="details-title">Total Bayar</div>
                                                    <div class="detail-amt font-weight-bolder">Rp. {{ number_format($total) }}</div>
                                                </li>
                                            </ul>
                                            <button type="button" class="btn btn-primary btn-block btn-next delivery-address mt-2">
                                                Bayar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- Checkout Payment Ends -->
                        <!-- </div> -->
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- END: Content-->
    
@endsection

@push('styles')
@endpush

@push('scripts')
<script>
$(document).ready(function () {
    $(document).on('click', '.del', function () {
        var id = $(this).data('id');
        Swal.fire({
            icon: 'error',
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'error',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        })
            .then((result) => {
                if (result.value) {
                    $.ajax({
                        'url': '{{url('cart')}}/' + id,
                        'type': 'post',
                        'data': {
                            '_method': 'DELETE',
                            '_token': '{{csrf_token()}}'
                        },
                        success: function (response) {
                            if (response == 1) {
                                toastr.success('Data berhasil dihapus!', 'Berhasil!', {
                                    closeButton: true,
                                    tapToDismiss: false
                                });
                                location.reload();
                            } else {
                                toastr.error('Data gagal dihapus!', 'Gagal!', {
                                    closeButton: true,
                                    tapToDismiss: false
                                });
                            }
                        }
                    });
                } else {
                    console.log(`dialog was dismissed by ${result.dismiss}`)
                }

            });
    });

    $(document).on('change', '#qty', function () {
        var id = $(this).data('id');
        var qty = $(this).val();
        var thisIs = $(this);
        $.ajax({
            'url': '{{url('cart')}}/' + id,
            'type': 'POST',
            'data': {
                'id': id,
                'qty': qty,
                'type': 3,
                '_method': 'PUT',
                '_token': '{{csrf_token()}}',
            },
            success: function (data) {
                $('#total-all').text('Rp. ' + data.total);
                // $('#harga-produk').text('Rp. ' + data.subtotal);
                $('#total').text('Rp. ' + data.total);
                var prices = thisIs.parents('div.ecommerce-card');
                prices.find('#harga-produk').text('Rp. ' + data.subtotal);
            }
        });
    });
});
</script>
@endpush


