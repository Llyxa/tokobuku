@extends('layouts.app')

@section('content')
<style>
section {
    /* background-image: url(/iniimg/bg1.png); */
    /* background-position: left; */
    /* background-repeat: no-repeat; */
    /* background-size: 800px 750px; */
    /* position: relative; */
    /* background-attachment: fixed; */
    background-color: #8e94f2;
    /* object-fit: scale-down; */
}

.tombol-login{
    background-color: #3C59A3;
    border: 2px solid #3C59A3;
}
.tombol-login:hover{
    background-color: #8e94f2;
    border: 2px solid #3C59A3;
    /* color: #3C59A3; */
    /* font-weight: 500; */
}
.reg{
    color: #3C59A3;
}
.reg:hover{
    color: #4566b9;
}
/* Color Pallate
D3D7EC
E9EBF6
3C59A3
F7F0F4
EFE6EA
CFB5D7
D8C2DF
6C74B5
C4B1D6
 */
</style>
<section class="vh-100">
    <div class="container-fluid h-custom">
        <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col-md-9 col-lg-6 col-xl-5">
                {{-- <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.webp" class="img-fluid" alt="Sample image"> --}}
                <img src="/iniimg/bg2.png" class="img-fluid mt-3" alt="Sample image" >
            </div> 
            <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
                <form method="POST" action="{{ route('login') }}">
                @csrf
                {{-- <div class="d-flex flex-row align-items-center justify-content-center justify-content-lg-start">
                    <p class="lead fw-normal mb-0 me-3">Sign in with</p>
                    <button type="button" class="btn btn-primary btn-floating mx-1">
                    <i class="fab fa-facebook-f"></i>
                    </button>
        
                    <button type="button" class="btn btn-primary btn-floating mx-1">
                    <i class="fab fa-twitter"></i>
                    </button>
        
                    <button type="button" class="btn btn-primary btn-floating mx-1">
                    <i class="fab fa-linkedin-in"></i>
                    </button>
                </div>
                <div class="divider d-flex align-items-center my-4">
                    <p class="text-center fw-bold mx-3 mb-0">Or</p>
                </div> --}}
        
                <!-- Email input -->
                <div class="form-outline mb-4">
                    <label class="form-label" for="email">Email address</label>
                    <input type="email" id="email" class="form-control form-control-lg @error('email') is-invalid @enderror"
                    placeholder="Enter a valid email address" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus/>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
        
                <!-- Password input -->
                <div class="form-outline mb-3">
                    <label class="form-label" for="password">Password</label>
                    <input type="password" id="password" class="form-control form-control-lg @error('password') is-invalid @enderror"
                    placeholder="Enter password" name="password" value="{{ old('password') }}" required autocomplete="current-password"/>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
        
                <div class="d-flex justify-content-between align-items-center">
                    <!-- Checkbox -->
                    <div class="form-check mb-0">
                    <input class="form-check-input me-2" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} />
                    <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                    </div>
                    @if (Route::has('password.request'))
                        <a href="{{ route('password.request') }}" class="text-body">
                            Forgot password?
                        </a>
                    @endif
                </div>
        
                <div class="text-center text-lg-start mt-4 pt-2">
                    <button type="submit" class="btn btn-primary tombol-login" style="padding-left: 2.5rem; padding-right: 2.5rem;">
                        {{ __('Login') }}
                    </button>
                    <p class="small fw-bold mt-2 pt-1 mb-0">Don't have an account? 
                        <a href="{{ route('register') }}" class="reg">
                            {{ __('Register') }}
                        </a>
                    </p>
                </div>
        
                </form>
            </div>
        </div>
    </div>
    
    {{-- <div
        class="d-flex flex-column flex-md-row text-center text-md-start justify-content-between py-4 px-4 px-xl-5 bg-primary">
        <!-- Copyright -->
        <div class="text-white mb-3 mb-md-0">
        Copyright © 2020. All rights reserved.
        </div>
        <!-- Copyright -->
    
        <!-- Right -->
        <div>
        <a href="#!" class="text-white me-4">
            <i class="fab fa-facebook-f"></i>
        </a>
        <a href="#!" class="text-white me-4">
            <i class="fab fa-twitter"></i>
        </a>
        <a href="#!" class="text-white me-4">
            <i class="fab fa-google"></i>
        </a>
        <a href="#!" class="text-white">
            <i class="fab fa-linkedin-in"></i>
        </a>
        </div>
        <!-- Right -->
    </div> --}}
</section>
    {{-- <div class="container">
        <div class="row justify-content-center" >
            <div class=" mt-5 b">
                <div class="card mt-5 a">
                    <div class="card-header">
                        <h3><b>Login</b></h3>
                    </div>

                    <div class="card-body" >
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="row mb-3">
                                <label for="email" class="col-md-4 col-form-label text-md-end">Alamat Email</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="password" class="col-md-4 col-form-label text-md-end">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Login') }}
                                    </button>

                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            Lupa Password?
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

@endsection

