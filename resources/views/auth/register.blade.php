@extends('layouts.app')

@section('content')
<style>
body {
    background-color: #8e94f2;
}
.tombol-reg{
    background-color: #3C59A3;
    border: 2px solid #3C59A3;
}
.tombol-reg:hover{
    background-color: #8e94f2;
    border: 2px solid #3C59A3;
    /* color: #3C59A3; */
    /* font-weight: 500; */
}
.log{
    color: #3C59A3;
}
.log:hover{
    color: #4566b9;
}
</style>
<section class="vh-100">
    <div class="container-fluid h-custom">
        <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col-md-9 col-lg-6 col-xl-5">
                {{-- <img src="/iniimg/bg1.png" class="img-fluid" alt="Sample image"> --}}
                <img src="/iniimg/bg2.png" class="img-fluid mt-3" alt="Sample image">
            </div>
            <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
                <form method="POST" action="{{ route('register') }}">
                @csrf
        
                <!-- Name input -->
                <div class="form-outline mb-4">
                    <label class="form-label" for="name">Your Name</label>
                    <input type="name" id="name" class="form-control form-control-lg @error('name') is-invalid @enderror"
                    placeholder="Enter a valid name address" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus/>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <!-- Email input -->
                <div class="form-outline mb-4">
                    <label class="form-label" for="email">Your Email</label>
                    <input type="email" id="email" class="form-control form-control-lg @error('email') is-invalid @enderror"
                    placeholder="Enter your email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus/>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
        
                <!-- Password input -->
                <div class="form-outline mb-3">
                    <label class="form-label" for="password">Password</label>
                    <input type="password" id="password" class="form-control form-control-lg @error('password') is-invalid @enderror"
                    placeholder="Enter password" name="password" value="{{ old('password') }}" required autocomplete="current-password"/>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <!-- Password Confirmation -->
                <div class="form-outline mb-3">
                    <label class="form-label" for="password-confirm">Password Confirmation</label>
                    <input type="password" id="password-confirm" class="form-control form-control-lg"
                    placeholder="Confirmation your password" name="password-confirmation" required autocomplete="new-password"/>
                </div>
        
                <div class="text-center text-lg-start mt-4 pt-2">
                    <button type="submit" class="btn btn-primary tombol-reg" style="padding-left: 2.5rem; padding-right: 2.5rem;"> 
                        {{ __('Register') }}
                    </button>
                    <p class="small fw-bold mt-2 pt-1 mb-0">Have an account? 
                        <a href="{{ route('login') }}" class="log">
                            {{ __('Login') }}
                        </a>
                    </p>
                </div>
        
                </form>
            </div>
        </div>
    </div>
</section>
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 mt-5">
            <div class="card mt-5">
                <div class="card-header">
                    <h3><b>Register</b></h3>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">Username</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="Masukkan Username" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">Alamat Email</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Masukkan Alamat Email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Masukkan Password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-end">Konfirmasi Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Masukkan Kembali Passwordmu">
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
