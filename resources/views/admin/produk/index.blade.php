@extends('admin.layouts.app')
@section('listbuku', 'active')
@section('title','Data produk')

@section('content')
<style>
    .card {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        text-align: center;
        transition: transform .2s;
    }
    .card:hover{
        transform: scale(1.03);
    }
    div.a{
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }
    div.a:hover{
        overflow: visible;
    }

    /* .row-cols-6 {
    width: 100%;
    height: 150px;
    overflow: visible;
    white-space:nowrap;
    }

    .row-cols-6 .card {
        display: inline-block;
    } */

</style>

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">List Buku</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a>
                                </li>
                                <li class="breadcrumb-item active">List Buku
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="app-todo.html"><i class="mr-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="mr-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="mr-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="mr-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body product_data">
            @can('admin')
            <div class="header">
                <a href="{{route('produk.create')}}" class="btn btn-primary waves-effect waves-float waves-light">Tambah produk</a>
            </div>
            @endcan
            <h6 class="my-2 text-muted">Baru Ditambahkan</h6>
            <section id="card-content-types">
                <div class="row row-cols-5">
                    {{-- {{dd($produk);}} --}}
                    @foreach ($produk as $item)
                    <div class="col">
                        @can('admin')
                        <div class="card h-90 mb-1">
                            <a href="{{route('produk.show', $item->id)}}">
                                <img class="card-img-top" src="{{ asset ('iniimg/foto/'. $item->image) }}" alt="{{$item->judul}}" style="object-fit: cover; border-radius: 6px; height:250px; padding: 10px;"/>
                            </a>
                            <div class="card-body mt-0">
                                <div class="card-text a">{{$item->penulis}}</div>
                                <div class="card-title font-weight-bolder mt-0 mb-1 a">{{$item->judul}}</div>
                                <p class="card-text">Rp. {{ number_format($item->harga) }}</p>
                            </div>
                            {{-- <a href="{{route('produk.edit', $product->id)}}" class="btn btn-primary btn-cart mr-0 mr-sm-1 mb-1 mb-sm-0">
                                <i data-feather="edit" class="mr-50"></i>
                                <span class="">Edit</span>
                            </a>
                            <a href="#" data-id="{{$product->id}}" class="btn btn-danger btn-del waves-effect waves-float waves-light">Delete</a> --}}
                        </div>
                        @endcan
                        @can('user')
                            <div class="card h-90 mb-2" style="padding: 10px;">
                                <a href="{{route('produk.show', $item->id)}}">
                                    <img class="card-img-top" src="{{ asset ('iniimg/foto/'. $item->image) }}" alt="Cover Image" style="object-fit: cover; border-radius: 6px; height:250px;"/>
                                </a>
                                <div class="card-body mt-0">
                                    <div class="card-text a">{{$item->penulis}}</div>
                                    <div class="card-title font-weight-bolder mt-0 mb-1 a">{{$item->judul}}</div>
                                    <p class="card-text">Rp. {{ number_format($item->harga) }}</p>
                                </div>
                                
                                <div class="text-center">
                                    <input type="hidden" name="user_id" value="{{ @ Auth::user()->id }}" class="user_id">
                                    <button type="button" class="btn btn-primary btn-cart btn-add-to-cart" id="btn-add-to-cart" data-id="{{$item->id}}">
                                        <i data-feather="shopping-cart"></i>
                                        <span class="add-to-cart">Add to cart</span>
                                    </button>
                                </div>
                            </div>
                        @endcan
                    </div>
                    @endforeach                    
                </div>
            </section>
        </div>
    </div>
</div>
@endsection

@push('styles')
@endpush

@push('scripts')
<script>
    $(document).ready(function () {
        $('.btn-add-to-cart').click(function () {
            var id = $(this).data('id');
            $.ajax({
                url: "{{route('cart.store')}}",
                type: "POST",
                data: {
                    product_id: id,
                    _token: '{{csrf_token()}}'
                },
                success: function (response) {
                    toastr.success(response.message, 'Berhasil!', {
                            closeButton: true,
                            tapToDismiss: false
                        });
                        $('#cart-list').html('');
                        let rows = ''
                        $.each(response.cart.cart, function (idx, d) { 
                            let disabled = "";
                            if(d.qty <= 1)
                                disabled = "disabled";
                                rows +=  '<div class="media align-items-center" id="cart-items">' +
                                            '<img class="d-block rounded mr-1" src="{{ asset("iniimg/foto")}}/'+ d.produk.image +'" width="40">' +
                                            '<div class="media-body">' +
                                                '<i id="btn-del-cart" class="ficon cart-item-remove" data-id="' +d.id+ '" data-feather="x"></i>' +
                                                '<div class="media-heading">' +
                                                    '<h6 class="cart-item-title"><a class="text-body" href="#">' +d.produk.judul+ '</a></h6>' +
                                                    '<small class="cart-item-by">'+d.produk.penulis+'</small>' +
                                                '</div>' +
                                                '<div class="cart-item-qty">' +
                                                    '<div class="input-group">' +
                                                        '<input class="touchspin-cart" id="quantity" data-id="'+ d.id +'" type="number" value="'+ d.qty +'" height="1rem">' +
                                                    '</div>' +
                                                '</div>' +
                                                '<h6 class="cart-item-price font-weight-1 price-item" id="price-item">Rp. '+ d.subtotal +'</h6>' +
                                            '</div>' +
                                        '</div>' ;
                        })
                        $('#cart-count').html((response.count));
                        $('#bulet').html((response.count));
                        $('#total').html('Rp'+(response.total));
                        $('#cart-list').html(rows);
                        var quantity = document.querySelectorAll('#quantity');
                        quantity.forEach(qty => {
                            $(qty).TouchSpin();
                        });                            
                        if (feather) {
                            feather.replace({
                                width: 14,
                                height: 14
                            });
                        }
                },
                error: function (data) {
                    toastr.error('Produk gagal ditambahkan', 'Gagal!', {
                        closeButton: true,
                        tapToDismiss: false
                    });
                }
            });
        });
    });
</script>
@endpush