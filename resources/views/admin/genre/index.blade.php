@extends('admin.layouts.app')
@section('genre', 'active')
@section('title','Data Genre')

@section('content')

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Data Genre</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Genre
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="app-todo.html"><i class="mr-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="mr-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="mr-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="mr-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic Tables start -->
            <div class="row" id="basic-table">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            {{-- <a href="{{route('genre.create')}}" class="btn btn-primary waves-effect waves-float waves-light">Tambah genre</a> --}}
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add-genre">
                                Tambah Genre
                            </button>
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Genre</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="list">
                                    {{-- @foreach($genre as $row)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$row->genre}}</td>
                                            <td>
                                                <a href="{{route('genre.edit', $row->id)}}" class="btn btn-warning waves-effect waves-float waves-light">Edit</a>
                                                <a href="#" data-id="{{$row->id}}" class="btn btn-warning waves-effect waves-float waves-light">Edit</a>
                                                <a href="#" data-id="{{$row->id}}" class="btn btn-danger btn-del waves-effect waves-float waves-light">Delete</a>
                                                <form action="{{ route('genre.destroy', $row->id) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach --}}
                                </tbody>
                            </table>
                        </div>
                        <!-- Modal Add -->
                        <div class="modal fade" id="add-genre" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Genre</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <input type="text" id="input-genre" class="form-control" name="genre" placeholder="Genre" value="" />
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary submit" id="submit">Submit</button>
                                </div>
                            </div>
                            </div>
                        </div>
                        <!-- Modal Update -->
                        <div class="modal fade" id="form-update-genre" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Edit Genre</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="edit-genre">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="edit-close">Cancel</button>
                                    <button type="submit" class="btn btn-primary" id="edit-submit">Submit</button>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Basic Tables end -->
        </div>
    </div>
</div>
@endsection

@push('styles')
@endpush

@push('scripts')
<script>
$(document).ready(function () {
    getGenre();

    $('#submit').on('click', function(){
        $.ajax({
            url: "{{route('genre.store')}}",
            type: "POST",
            data: {
                genre: $('#input-genre').val(),
                _token: '{{csrf_token()}}'
            },
            success: function (response) {
                if (response.code === 200) {
                    $('#input-genre').val('');
                }
                $(document).find('#add-genre').find('#close').click();
                getGenre();
                if(response.code == 200){
                    return toastr.success(response.message, 'Success!', {
                        closeButton: true,
                        tapToDismiss: true
                    });
                }
                $.each(response.error, function (idx, err) { 
                        toastr.error(err, 'Error!', {
                        closeButton: true,
                        tapToDismiss: true
                    });
                });
            }

        });
    });

    $(document).on('click', '#update-btn', function() {
        event.preventDefault();
        const thisIs = $(this);
        const id = $(this).data('id');
        $.ajax({
            type: "GET",
            url: "{{ url('genre') }}/"+id,
            dataType: "JSON",
            success: function (response) {
                $('#edit-genre').val(response.data.genre);
                $(thisIs).parents(document).find('#edit-submit').on('click', function(){
                    $.ajax({
                        type: "POST",
                        url: "{{ url('genre') }}/"+id,
                        data: {
                            'genre': $('#edit-genre').val(),
                            '_method': 'PUT',
                            '_token': '{{ csrf_token() }}'
                        },
                        success: function (response) {
                            $(thisIs).parents(document).find('#form-update-genre').find('#edit-close').click();
                            getGenre();
                            if(response.code === 200){
                                
                                return toastr.success(response.message, 'Success!', {
                                    closeButton: true,
                                    tapToDismiss: true
                                });
                            }
                            $.each(response.error, function (idx, err) { 
                                toastr.error(err, 'Error!', {
                                    closeButton: true,
                                    tapToDismiss: true
                                });
                            });
                        }
                    });
                });
            }
        });
    });

    $(document).on('click', '#del-btn', function () {
        var id = $(this).data('id');
        Swal.fire({
            icon: 'error',
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'error',
            showCancelButton: true,
            confirmButtonColor: '#28C76F',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        })
        .then((result) => {
            if (result.value) {
                $.ajax({
                    'url': '{{url('genre')}}/' + id,
                    'type': 'POST',
                    'data': {
                        '_method': 'DELETE',
                        '_token': '{{csrf_token()}}'
                    },
                    success: function (response) {
                        if (response.message) {
                            getGenre();
                            return toastr.success(response.message, 'Success!', {
                                closeButton: true,
                                tapToDismiss: true
                            });
                        }
                            getGenre();
                            return toastr.error('Failed!', 'Failed!', {
                                closeButton: true,
                                tapToDismiss: true
                            });
                    }
                });
            } else {
                console.log(`dialog was dismissed by ${result.dismiss}`)
            }
        });
    });


    function getGenre() {
        $.ajax({
            type: "GET",
            url: "{{ url('genreindex') }}",
            dataType: "JSON",
            success: function (response) {
                let rows = '';
                $.each(response.datas, function (idx, data) { 
                    idx++
                    rows += '<tr>'+
                                '<td>'+idx+'</td>'+
                                '<td>'+data.genre+'</td>'+
                                '<td>'+
                                    '<a href="#" data-id="'+data.id+'" data-toggle="modal" id="update-btn" data-target="#form-update-genre" class="btn btn-warning waves-effect waves-float waves-light mr-1">Edit</a>'+
                                    '<a href="#" data-id="'+data.id+'" id="del-btn" class="btn btn-danger btn-del waves-effect waves-float waves-light">Delete</a>'+
                                '</td>'+
                            '</tr>';
                });
                $('#list').html('');
                $('#list').append(rows);
                if (feather) {
                    feather.replace({
                        width: 14,
                        height: 14
                    });
                }
            }
        });
    }
});
</script>
@endpush