<!-- BEGIN: Vendor JS-->
<script src="{{asset('assets')}}/admin/vendors/js/vendors.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="{{asset('assets')}}/admin/vendors/js/charts/apexcharts.min.js"></script>
<script src="{{asset('assets')}}/admin/vendors/js/extensions/toastr.min.js"></script>
<script src="{{asset('assets')}}/admin/vendors/js/extensions/sweetalert2.all.min.js"></script>
<script src="{{asset('assets')}}/admin/vendors/js/forms/repeater/jquery.repeater.min.js"></script>
<script src="{{asset('assets')}}/admin/vendors/js/forms/wizard/bs-stepper.min.js"></script>
<script src="{{asset('assets')}}/admin/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{asset('assets')}}/admin/js/core/app-menu.js"></script>
<script src="{{asset('assets')}}/admin/js/core/app.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
@stack('pagejs')
{{-- <script src="{{asset('assets')}}/admin/js/scripts/pages/dashboard-ecommerce.js"></script> --}}
<script src="{{asset('assets')}}/admin/js/scripts/forms/form-repeater.js"></script>
<script src="{{asset('assets')}}/admin/js/scripts/pages/app-ecommerce-checkout.js"></script>
<!-- END: Page JS-->

<script src="{{ asset('inijs/myFunc.js') }}"></script>

<script>
$(document).ready(function () {
    $('#cart-list').on('click', '#btn-del-cart', function () {
        var id = $(this).data('id');
        Swal.fire({
            icon: 'error',
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'error',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        })
        .then((result) => {
            if (result.value) {
                $.ajax({
                    'url': '{{url('cart')}}/' + id,
                    'type': 'POST',
                    'data': {
                        '_method': 'DELETE',
                        '_token': '{{csrf_token()}}'
                    },
                    success: function (response) {
                        if (response.message) {
                            getCart();
                            return toastr.success(response.message, 'Success!', {
                                closeButton: true,
                                tapToDismiss: true
                            });
                        }
                            getCart();
                            return toastr.error('Failed!', 'Failed!', {
                                closeButton: true,
                                tapToDismiss: true
                            });
                    }
                });
            } else {
                console.log(`dialog was dismissed by ${result.dismiss}`)
            }
        });
    });
    
    function getCart() {
        $.ajax({
            type: "GET",
            url: "{{ url('cartindex') }}",
            dataType: "JSON",
            success: function (response) {
                let rows = '';
                $.each(response.datas, function (idx, d) { 
                    rows += '<div class="media align-items-center" id="cart-items">' +
                                '<img class="d-block rounded mr-1" src="{{ asset("iniimg/foto")}}/'+d.produk.image+ ' " width="40">' +
                                '<div class="media-body">' +
                                    '<i id="btn-del-cart" class="ficon cart-item-remove" data-id="' +d.id+ '" data-feather="x"></i>' +
                                    '<div class="media-heading">' +
                                        '<h6 class="cart-item-title "><a class="text-body" href="#">' +d.produk.judul+ '</a></h6>' +
                                        '<small class="cart-item-by">' +d.produk.penulis+ '</small> ' +
                                    '</div>' +
                                    '<div class="cart-item-qty">' +
                                        '<div class="input-group">' +
                                            '<input class="touchspin-cart" id="quantity" data-id="' +d.id+ '" type="number" value="' +d.qty+ '" height="1rem">' +
                                        '</div>' +
                                    '</div>' +
                                    '<h6 class="cart-item-price font-weight-1 price-item" id="price-item">Rp.' +d.subtotal+ '</h6>' +
                                '</div>' +
                            '</div>' + 
                            '<div id="empty-note"></div>';
                });
                $('#cart-list').html('');
                $('#cart-list').append(rows);
                $('#total').html('Rp'+(response.total));
                $('#cart-count').html((response.count));
                if (feather) {
                    feather.replace({
                        width: 14,
                        height: 14
                    });
                }
            }
        });
    }

    $(document).on('change', '#quantity', function () {
        var id = $(this).data('id');
        var qty = $(this).val();
        var thisIs = $(this);
        $.ajax({
            'url': '{{url('cart')}}/' + id,
            'type': 'POST',
            'data': {
                'id': id,
                'qty': qty,
                'type': 3,
                '_method': 'PUT',
                '_token': '{{csrf_token()}}',
            },
            success: function (data) {
                $('#total').text('Rp. ' + data.total);
                var prices = thisIs.parents('div.media-body');
                prices.find('#price-item').text('Rp. ' + data.subtotal);
            }
        });
    });
});
</script>

<script>
$('#exampleModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('whatever') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('.modal-title').text('New message to ' + recipient)
    modal.find('.modal-body input').val(recipient)
    })
</script>

@stack('script')
@stack('scripts')
<script>
$(window).on('load', function() {
if (feather) {
    feather.replace({
        width: 14,
        height: 14
    });
}
});
</script>
