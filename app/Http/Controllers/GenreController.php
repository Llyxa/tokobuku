<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Genre;
use Psy\Util\Str;

class GenreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['genre'] = Genre::all();
        return view('admin.genre.index', $data);
    }

    public function isigenre()
    {
        return response()->json([
            'message' => 'List of Genre',
            'datas' => Genre::all(),
        ]);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return "Hello World";
        return view('admin.genre.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        //Start Validation
        $rules = [
            'genre' => 'required|unique:genres',
        ];

        $customMessages = [
            'genre.required' => 'Genre wajib diisi!',
            'genre.unique' => 'Genre sudah digunakan!',
        ];

        $this->validate($request, $rules, $customMessages);

        //Start Input
        $input = $request->all();
        $data = Genre::create($input);

        return response()->json([
            'code' => 200,
            'message' => 'Tambah genre berhasil!',
            'data' => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'message' => 'Success finding data genre!',
            'data' => Genre::findOrFail($id),
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['genre'] = Genre::find($id);
        return view('admin.genre.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Start Validation
        $rules = [
            'genre' => 'required|unique:genres',
        ];

        $customMessages = [
            'genre.required' => 'Genre wajib diisi!',
            'genre.unique' => 'Genre sudah digunakan!',
        ];

        $this->validate($request, $rules, $customMessages);

        //Start Input
        $genre = Genre::find($id);
        $update = $request->all();
        $status = $genre->update($update);

        return response()->json([
            'code' => 200,
            'message' => 'Success to update genre!',
            'data' => $genre
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Genre $genre)
    {
        $genre->findOrFail($genre->id)->delete($genre);
        return response()->json([
            'message' => 'Success to delete genre!'
        ]);

    }

}
